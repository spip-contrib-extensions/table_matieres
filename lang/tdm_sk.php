<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tdm?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => 'Chcete, aby obsah vytváral automaticky?',
	'automatique_desription' => 'Túto možnosť deaktivujete, ak do šablón dáte 
		tag <code>#TABLE_MATIERES.</code>',

	// E
	'explication_longueur' => 'Nastaví maximálny počet znakov, ktorý bude platiť každú kotvu.',
	'explication_min' => 'Nastaví minimálny počet nadpisov v texte, z ktorých sa vytvorí obsah.',
	'explication_separateur' => 'Určí znak, ktorý sa vloží medzi slová namiesto medzier.',
	'explication_tdm_flottante' => 'Keď sa pri posúvaní obrazovky obsah stratí z dohľadu, umiestni sa do hornej časti obrazovky, aby ho bolo stále vidieť.',

	// L
	'label_tdm_flottante' => 'Tabuľka s „plávajúcim” obsahom',
	'longueur' => 'Dĺžka:',

	// M
	'min_intertitres' => 'Minimálny počet titulkov',

	// R
	'retour_table_matiere' => 'Vrátiť sa na obsah',

	// S
	'separateur' => 'Oddeľovač:',

	// T
	'table_matiere' => 'Obsah'
);
