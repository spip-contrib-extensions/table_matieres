<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/table_matieres.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => 'La table des matières est-elle générée automatiquement ?',
	'automatique_desription' => 'Désactivez cette option si vous placez vous-même
		la balise <code>#TABLE_MATIERES</code> dans vos squelettes.',

	// E
	'explication_longueur' => 'Définit le nombre de caractères maximum que constituera chaque ancres.',
	'explication_min' => 'Définit le nombre minimal d’intertitres d’un texte à partir duquel une table des matières sera affichée.',
	'explication_separateur' => 'Définit le caractère qui s’intercalera entre chaque mot pour remplacer les espaces.',
	'explication_tdm_flottante' => 'Lorsque la table des matières devient invisible lors du scroll de la page, celle-ci se positionne en haut de l’écran afin d’être constamment visible.',

	// L
	'label_tdm_flottante' => 'Table des matières flottante',
	'longueur' => 'Longueur :',

	// M
	'min_intertitres' => 'Intertitres minimum ',

	// R
	'retour_table_matiere' => 'Retour à la table des matières',

	// S
	'separateur' => 'Séparateur :',

	// T
	'table_matiere' => 'Table des matières'
);
