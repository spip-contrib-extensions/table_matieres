<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tablematieres?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tablematieres_description' => 'Koppelt een anker aan iedere tussentitel van het veld #TEXTE van een artikel,
	alsmede een tag #TABLE_MATIERES (facultatief) die binnen een ARTICLES-lus kan worden opgenomen.',
	'tablematieres_nom' => 'Inhoudsopgave',
	'tablematieres_slogan' => 'Een overzicht van een artikel maken'
);
