<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tablematieres?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tablematieres_description' => 'Introduce un ancla transcrita para cada encabezado del campo #TEXTE de los ARTÍCULOS,
	así como una etiqueta #TABLE_MATIERES (opcional) asociada a placer, seguro, en un bucle ARTICLES.',
	'tablematieres_nom' => 'Tabla de Materias',
	'tablematieres_slogan' => 'Crear el resumen de un artículo'
);
