<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/table_matieres.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tablematieres_description' => 'Introduit une ancre translitterée pour chaque intertitre du champ #TEXTE des ARTICLES,
	ainsi qu’une balise #TABLE_MATIERES (facultative) associée à placer, bien sûr, dans une boucle ARTICLES.',
	'tablematieres_nom' => 'Table des Matières',
	'tablematieres_slogan' => 'Créer le sommaire d’un article'
);
