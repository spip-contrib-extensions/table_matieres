<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tdm?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => '¿El sumario se ha generado automáticamente?',
	'automatique_desription' => 'Desactive esta opción si indica usted mismo la etiqueta <code>#TABLE_MATIERES</code> en sus esqueletos.',

	// E
	'explication_longueur' => 'Definido el número de caracteres máximo que constituirá cada anclaje.',
	'explication_min' => 'Definido el número mínimo de epígrafes de un texto a partir del cual un sumario se mostrará.',
	'explication_separateur' => 'Definido el carácter que se intercalará entre cada palabra para remplazar los espacios.',
	'explication_tdm_flottante' => 'Cuando el índice de contenidos se torna invisible al desplazarse por la página, éste se posiciona en la parte superior de la pantalla para estar constantemente visible. ',

	// L
	'label_tdm_flottante' => 'Sumario flotante',
	'longueur' => 'Longitud:',

	// M
	'min_intertitres' => 'Epígrafes (subtítulos) mínimos ',

	// R
	'retour_table_matiere' => 'Volver al sumario',

	// S
	'separateur' => 'Separador:',

	// T
	'table_matiere' => 'Sumario'
);
