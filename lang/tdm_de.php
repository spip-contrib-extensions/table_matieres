<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tdm?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => 'Inhaltsverzeichnis automatisch erstellen?',
	'automatique_desription' => 'Bitte deaktivieren, falls sie den
	SPIP-Tag <code>#TABLE_MATIERES</code> direkt in ihre
	SPIP-Skelette einfügen.',

	// E
	'explication_longueur' => 'Legt die maximale Zeichenzahl für jeden Anker fest.',
	'explication_min' => 'Legit die Mindestanzahl der Zwischentitel fest, ab der ein Inhaltsverzeichnis eingeblendet wird.',
	'explication_separateur' => 'Legt das Zeichen fest, daß anstelle von Leerzeichen zwischen Worte eigefügt wird.',
	'explication_tdm_flottante' => 'Wenn das Inhaltsverzeichnis durch Verschieben des sichtbaren Teils der Seite verdeckt würde, positioniert es sich am obenen Rand der Seite, um jedezeit eichtbar zu sein..',

	// L
	'label_tdm_flottante' => 'Schwebendes Inhaltsverzeichnis',
	'longueur' => 'Länge:',

	// M
	'min_intertitres' => 'Mindestanzahl Zwischentitel',

	// R
	'retour_table_matiere' => 'Zurück zum Inhaltsverzeichnis',

	// S
	'separateur' => 'Trennzeichen:',

	// T
	'table_matiere' => 'Inhaltsverzeichnis'
);
