<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tdm?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'automatique' => 'Wordt de inhoudsopgave automatisch gegenereerd?',
	'automatique_desription' => 'Desactiveer deze optie wanneer je zelf de tag
		<code>#TABLE_MATIERES</code> in je skeletten opneemt.',

	// E
	'explication_longueur' => 'Bepaalt het maximum aantal tekens voor ieder anker.',
	'explication_min' => 'Geeft aan na hoeveel tussentitels de inhoudsopgave moet worden geplaatst.',
	'explication_separateur' => 'Bepaalt welk teken wordt gebruikt ter vervanging voor een spatie.',
	'explication_tdm_flottante' => 'Wanneer de inhoudsopgave tijdens het scrollen op een bladzijde onzichtbaar wordt, plaatst hij zich bovenaan het scherm om continu zichtbaar te blijven.',

	// L
	'label_tdm_flottante' => 'Zwevende inhoudsopgave',
	'longueur' => 'Lengte:',

	// M
	'min_intertitres' => 'Minimum aantal tussentitels ',

	// R
	'retour_table_matiere' => 'Terug naar de inhoudsopgave',

	// S
	'separateur' => 'Scheidingsteken:',

	// T
	'table_matiere' => 'Inhoudsopgave'
);
